/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * @author Emiliano Riffel
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/main.h"       /* <= own header */
#include "uart.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "bool.h"
#include "Tcrt5000.h"
#include "analog_io.h"
#include "timer.h"
#include "balanza.h"
/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0
#define INTERVALO_CONTADOR 1 //ms
#define VELOCIDAD_TRANSFERENCIA 115200
#define PUERTO_PC SERIAL_PORT_PC
#define CANAL_ANALOGICO CH1
#define MODO_ANALOGICO AINPUTS_SINGLE_READ
#define PESO_CAJA_LLENA 20
#define LOTE_COMPLETO 15

/*==================[internal functions declaration]=========================*/
void Salvar_tiempo_llenado();
void Informar_tiempo_llenado();
void Estado_del_lote();
void Contar();
/*==================[internal data definition]===============================*/

 bool estado_sistema = ON;
 bool llenando_caja  = FALSE;

 uint8_t cajas_llenadas = 0;


 uint32_t tiempo_llenado = 0;
 uint32_t tiempo_maximo_llenado = 0;
 uint32_t tiempo_minimo_llenado = 99999;


 // Timer para contar tiempo de llenado
 timer_config contador_timer = {
 								TIMER_A,
								INTERVALO_CONTADOR,
 								&Contar
 							  };
// Configuación del puerto serie
 serial_config config_serie = {
 						 PUERTO_PC,
 						 VELOCIDAD_TRANSFERENCIA,
 						 NULL,
 					   };
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


// Funcion que envía por puerto serie los tiempos máximo y mínimo del lote
 void Informar_tiempos_lote(){
	UartSendString(config_serie.port,"Tiempo de llenado máximo: ");
	UartSendString(config_serie.port,UartItoa(tiempo_maximo_llenado,10));
	UartSendString(config_serie.port," seg. \r\n");

	UartSendString(config_serie.port,"Tiempo de llenado mínimo: ");
	UartSendString(config_serie.port,UartItoa(tiempo_minimo_llenado,10));
	UartSendString(config_serie.port," seg. \r\n");
}

 // Funcion que revisa el estado del lote e informa si se ha alcanzado el total
 void Estado_del_lote(){

 	if(cajas_llenadas == LOTE_COMPLETO) // Se controla si se llegó al total de un lote
 	{
 		Informar_tiempos_lote(); // Se informa por puerto serie los tiempos del lote
 		cajas_llenadas = 0;
 	}
 }

 // Funcion que compara y almacena el tiempo de llenado según corresponda
 void Salvar_tiempo_llenado(){

	 if (tiempo_llenado > tiempo_maximo_llenado)
	 {
		 tiempo_maximo_llenado = tiempo_llenado;
	 }
	 else if(tiempo_llenado < tiempo_minimo_llenado){

		 tiempo_minimo_llenado = tiempo_llenado;
	 }
 }
 // Funcion que envía por puerto serie el tiempo de llenado de la caja actual
 void Informar_tiempo_llenado(){

	UartSendString(config_serie.port,"Tiempo de llenado de la caja ");
	UartSendString(config_serie.port,UartItoa(cajas_llenadas,10));
	UartSendString(config_serie.port," :");
	UartSendString(config_serie.port,UartItoa(tiempo_llenado/1000,10));
	UartSendString(config_serie.port," seg. \r\n");
}
 // Funcion que ejecuta el llenado de la caja
 void Llenar_caja(){

	 Balanza_init(CANAL_ANALOGICO,MODO_ANALOGICO);

	 TimerInit(&contador_timer);
	 TimerStart(TIMER_A);

	 // Se realiza la medición de la balanza hasta que se alcanza el valor máximo del peso de la caja
	 do {
		 Balanza_medir();
	 }
	 while(Balanza_obtener_peso() <= PESO_CAJA_LLENA);

 }

 // Funcion que sigue el tiempo de llenado de una caja
 void Contar()
 {
	 if (llenando_caja){

		 tiempo_llenado++;
	 }
 }

 // Funciones que controlan el estado de sistema
 void Sistema_ON(){

 	estado_sistema = ON;
 }

 void Sistema_OFF(){

  	estado_sistema = OFF;
  }


int main(void){

	SystemClockInit();

	// Inicialización del sensor infrarrojo
	gpio_t gpio_infrarrojo = GPIO_T_FIL2;
	Tcrt5000Init(gpio_infrarrojo);

	// Inicialización puerto serie
	UartInit(&config_serie);

	// Inicialización teclas
	SwitchesInit();
	SwitchActivInt(SWITCH_1, &Sistema_ON);
	SwitchActivInt(SWITCH_2, &Sistema_OFF);

	// Inicialización de los Leds
	LedsInit();
	LedOn(LED_1); // se enciende la cinta 1

		while(1){

			if (estado_sistema == ON) {

				if (Tcrt5000State()){  // se comprueba si la caja llego a la posición de llenado

					LedOff(LED_1); // se detiene la cinta 1
					LedOn(LED_2); // se enciende la cinta 2

					llenando_caja = TRUE;

					Llenar_caja();

					llenando_caja = FALSE;

					Salvar_tiempo_llenado();   // Se registran los tiempos de llenado
					Informar_tiempo_llenado(); // Se informan los tiempos por puerto serie

					cajas_llenadas++;

					Estado_del_lote(); // Se revisa si se llego al final de un lote

					tiempo_llenado = 0;

					LedOn(LED_1); // se enciende la cinta 1
					LedOff(LED_2); // se apaga la cinta 2
				}
		}
	}
	return 0;
}

/*==================[end of file]============================================*/

