/*! @mainpage Sistema para el control de cintas transportadoras
 *
 * \section genDesc Descripción general
 *
 * El programa controla el sistema de llenado de cajas en una línea de producción
 *
 * @author Emiliano Riffel
 *
 * * \section hardConn Conexión de Hardware
 *
 * |   Tcrt5000    	     |   EDU-CIAA    |
 * |:-------------------:|:--------------|
 * | 	(ver comentario) | 	T_FIL2	     |
 * | 	VCC 	 		 | 	5V			 |
 * | 	GND 	 		 | 	GND			 |
 *
 * |   Balanza analógica 				 |   EDU-CIAA	|
 * |:-----------------------------------:|:-------------|
 * | 	GND  	 	        			 | 	GNDA     	|
 * | 	Pin_salida_analogica 	 	     | 	CH1  		|
 *
 * Comentario: no recuerdo el nombre del pin del sensor
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef main_H */

