/*! @mainpage Medidor de distancia con ultrasonido
 *
 * \section genDesc General Description
 *
 * La aplicación realiza una lectura de distancia con un sensor de ultrasonido cada 1 segundo
 * y envía estos resultados hacia un display y un puerto serie.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 24/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Emiliano Riffel
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Main.h"       /* <= own header */
#include "hc_sr4.h"
#include "led.h"
#include "systemclock.h"
#include "switch.h"
#include "uart.h"
#include <stdio.h>
#include "timer.h"
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/

#define VELOCIDAD_TRANSFERENCIA 115200
#define PERIODO_MEDICION 1000
#define DURACION_BLINK 1000
#define CM2  2
#define CM10  10
#define CM20  20
#define CM30  30
#define CM130 130

/*==================[internal functions declaration]=========================*/
void Tomar_medicion();
void Leer_Uart();
void Blink_Leds();
/*==================[internal data definition]===============================*/


// Almacena la lectura del sensor de ultrasonido
uint16_t lectura_sensor = 0;

// Almacena la tecla recibida por el puerto serie
uint8_t tecla_recibida_uart;

// Flag para el On/Off de la medición
bool midiendo = false;

// Flag para retener la medición
bool hold = false;

// Configuración para el sensor de ultrasonido
gpio_t echo = GPIO_T_FIL2;
gpio_t trigger = GPIO_T_FIL3;

// Configuración para el timer que controla la medición
timer_config medicion_timer = {
								TIMER_A,
							    PERIODO_MEDICION,
								&Tomar_medicion
							  };

// Timer para el efecto de los leds cuando la medición está fuera de rango
timer_config leds_timer = {
							TIMER_B,
							DURACION_BLINK,
							&Blink_Leds
						  };

// Configuración para el display
gpio_t pins [7] = {
					GPIO_LCD_1,
					GPIO_LCD_2,
					GPIO_LCD_3,
					GPIO_LCD_4,
					GPIO_1,
					GPIO_3,
					GPIO_5
				  };

// Configuración para el puerto serie
serial_config config_serie = {
						 SERIAL_PORT_PC,
						 VELOCIDAD_TRANSFERENCIA,
						 &Leer_Uart,
					   };

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


//
void Blink_Leds(void)
{
		LedToggle(LED_1);
		LedToggle(LED_2);
		LedToggle(LED_3);
		LedToggle(LED_RGB_B);
}

//  Función que setea el display y los leds para el estado desocupado
void IDLE_medicion() {

	LedsOffAll();
	ITSE0803DisplayValue(0);
	TimerStop(TIMER_A);

}

// Función que activa o desactiva la medición
void Medicion_on_off(){

	midiendo = !midiendo;

	if (midiendo){
		// Se inicializa el timer
		TimerInit(&medicion_timer);
		TimerStart(TIMER_A);
	}
	else
	{
		IDLE_medicion();
	}

}

// Función que mantiene el valor de medición del sensor
void Hold_medicion(){

	hold = !hold;
}

// Función que toma y ejecuta las mediciones
void Tomar_medicion(){

		if (hold)
			{
				// Se saca la medición por el display
				ITSE0803DisplayValue(lectura_sensor);
			}
		else{

			lectura_sensor = HcSr04ReadDistanceInCentimeters();

			// Se controla si la lectura está fuera de rango
			if (lectura_sensor < CM2 || lectura_sensor > CM130)
				{

					TimerInit(&leds_timer);
					TimerStart(TIMER_B);
					UartSendString(config_serie.port,"FUERA DE RANGO\r\n");
					ITSE0803DisplayValue(999);
				}
			else
			{

				ITSE0803DisplayValue(lectura_sensor);
				TimerStop(TIMER_B);

				// Se saca la medición por el puerto serie
				UartSendString(config_serie.port,UartItoa(lectura_sensor,10));
				UartSendString(config_serie.port," cm \r\n");

				if(lectura_sensor < CM10)
					{
						LedsOffAll();
						LedOn(LED_RGB_B);
					}
				if (lectura_sensor > CM10 && lectura_sensor < CM20)
					{
						LedsOffAll();
						LedOn(LED_RGB_B);
						LedOn(LED_1);
					}
				if (lectura_sensor > CM20 && lectura_sensor < CM30)
					{
						LedsOffAll();
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
					}
				if (lectura_sensor > CM30)
					{
						LedsOffAll();
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOn(LED_3);
					}

			}
		}

}

void Leer_Uart(){

	// Se lee la tecla enviada desde la consola
	UartReadByte(config_serie.port,&tecla_recibida_uart);

	// Se envía la tecla recibida hacia la consola solo por cuestiones de debug
	UartSendString(config_serie.port,"Tecla enviada: ");
	UartSendString(config_serie.port,UartItoa(tecla_recibida_uart,10));
	UartSendString(config_serie.port,"\r\n");


	switch (tecla_recibida_uart)
	{
	//tecla O reemplaza tecla 1
		case 'O':
			Medicion_on_off();
			break;
	//tecla H reemplaza tecla 2
		case 'H':
			Hold_medicion();
			break;
		default:
			break;
	}
}

int main(void){

	SystemClockInit();
	LedsInit();

	// Inicialización teclas
	SwitchesInit();


	// Inicialización puerto serie
	UartInit(&config_serie);

	// Inicialización sensor ultrasonido
	HcSr04Init(echo,trigger);

	// Inicialización display
	ITSE0803Init(pins);


	// Teclas
	SwitchActivInt(SWITCH_1, &Medicion_on_off);
	SwitchActivInt(SWITCH_2, &Hold_medicion);

	while(1)
    {

    }
    
	return 0;
}

/*==================[end of file]============================================*/

