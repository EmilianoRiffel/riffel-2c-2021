/*! @mainpage Guía de Proyecto 3
 *
 * \section genDesc Descripción General
 *
 * El programa utiliza un sensor de ultrasonido para medir distancia y envía el valor hacia un display LCD y un puerto serie.
   Se utilizan interrupciones para el manejo de los eventos.
 *
 * \section hardConn Conexión del Hardware
 *
 * |  Sensor HCSR4	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5			|
 * | 	GND		 	| 	GND			|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIG	 	| 	T_FIL3		|
 *
 *
 * @section changelog Registro de cambios
 *
 * |   Fecha	| Descripción                                  	 |
 * |:----------:|:-----------------------------------------------|
 * | 24/09/2021 | Creación del proyecto	                         |
 * | 01/09/2021 | Finalización del proyecto                      |
 * @author Emiliano Riffel
 *
 */

#ifndef MAIN_H
#define MAIN_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

