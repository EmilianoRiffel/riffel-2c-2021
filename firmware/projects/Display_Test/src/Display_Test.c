/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Display_Test.h"       /* <= own header */
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	gpio_t pins [7] = {
			GPIO_LCD_1,
			GPIO_LCD_2,
			GPIO_LCD_3,
			GPIO_LCD_4,
			GPIO_1,
			GPIO_3,
			GPIO_5
	};

    ITSE0803Init(pins);

    ITSE0803DisplayValue(256);

    while(1){


	}
    
    ITSE0803Deinit(pins);
	return 0;
}

/*==================[end of file]============================================*/

