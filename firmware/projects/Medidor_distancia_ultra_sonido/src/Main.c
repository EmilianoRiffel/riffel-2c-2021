/*! @mainpage Medidor de distancia con ultrasonido
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 17/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Emiliano Riffel
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Main.h"       /* <= own header */
#include "hc_sr4.h"
#include "led.h"
#include "systemclock.h"
#include "switch.h"
#include "uart.h"
#include <stdio.h>
/*==================[macros and definitions]=================================*/

#define COUNT_DELAY 3000000
#define CM10 10
#define CM20 20
#define CM30 30
/*==================[internal data definition]===============================*/


uint16_t medicion = 0;
bool midiendo = true;
bool hold = false;

/*==================[internal functions declaration]=========================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	LedsInit();

	// Manejo de teclas de la placa
	SwitchesInit();
	uint8_t teclas;

	// Configuración para el puerto serie
	serial_config config = {
			SERIAL_PORT_PC,
			9600,
			NULL
	};
	UartInit(&config);

	// Configuración para el sensor de ultrasonido
	gpio_t echo = GPIO_T_FIL2;
	gpio_t trigger = GPIO_T_FIL3;
	HcSr04Init(echo,trigger);

	// Configuración para el display
	gpio_t pins [7] = {
				GPIO_LCD_1,
				GPIO_LCD_2,
				GPIO_LCD_3,
				GPIO_LCD_4,
				GPIO_1,
				GPIO_3,
				GPIO_5
		};
	ITSE0803Init(pins);



    while(1)
    {

    	// Delay para ver los datos sacados por el puerto serie de forma clara
    	DelayMs(500);

    	if (midiendo)
    	{

			if (!hold)
				{
					medicion = HcSr04ReadDistanceInCentimeters();
					// Se saca la medicion por el puerto serie
					UartSendString(config.port,UartItoa(medicion,10));
					UartSendString(config.port,"\r\n");
					// Se saca la medicion por el display
					ITSE0803DisplayValue(medicion);
				}
			if (medicion == 0)
				{
					LedsOffAll();
				}
			if(medicion < CM10)
				{
					LedsOffAll();
					LedOn(LED_RGB_B);
				}
			if (medicion > CM10 && medicion < CM20)
				{
					LedsOffAll();
					LedOn(LED_RGB_B);
					LedOn(LED_1);
				}
			if (medicion > CM20 && medicion < CM30)
				{
					LedsOffAll();
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
				}
			if (medicion > CM30)
				{
					LedOn(LED_3);
				}
    	}
    	else
    	{
    		LedsOffAll();
			ITSE0803DisplayValue(0);
    	}

	    teclas  = SwitchesRead();
    	switch(teclas)
    	{
    		case SWITCH_1:
    			midiendo = !midiendo;
    			DelayMs(300);
    			break;
    		case SWITCH_2:
    			hold = !hold;
    			DelayMs(300);
    			break;
    	}
    }
    
	return 0;
}

/*==================[end of file]============================================*/

