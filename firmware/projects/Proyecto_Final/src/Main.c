/*! @mainpage Proyecto Integrador
 *
 * \section hardConn Conexión de Hardware
 *
 * |   HC-05    	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	RX   	 	| 	RS232_TX	|
 * | 	TX  	 	| 	RS232_RX	|
 * | 	VCC 	 	| 	5V			|
 * | 	GND 	 	| 	GND			|
 *
 * |   Placa biopotenciales 	|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	VCC   	 	            | 	5V		    |
 * | 	GND  	 	            | 	GNDA     	|
 * | 	ECG 	 	            | 	CH1  		|
 *
 *
 * @section changelog Registro de cambios
 *
 * |   Fecha    | Descripción                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/10/2021 | Creación del documento		                 |
 * | 15/11/2021 | Entrega del documento		           		     |
 *
 * @author Emiliano Riffel
 *
 */

/*==================[inclusions]=============================================*/
#include "analog_io.h"
#include "uart.h"
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include <math.h>

#include "../../Proyecto_Final/inc/Proyecto_Final.h"       /* <= own header */
/*==================[internal functions declaration]=========================*/

void Leer_dato();
void Color_Indicador(uint8_t estado);
/*==================[macros and definitions]=================================*/

// Constantes para la lectura analógica
#define CANAL_ANALOGICO CH1
#define MODO_ANALOGICO AINPUTS_SINGLE_READ
#define INTERVALO_MUESTREO 4 // ms

// Constantes para la comunicación por puerto serie
#define PUERTO_PC SERIAL_PORT_PC
#define PUERTO_BLUETOOTH SERIAL_PORT_P2_CONNECTOR
#define VELOCIDAD_TRANSFERENCIA 115200

// Constantes utilizadas para el manejo del color del indicador
#define ONDA_R 1
#define IDLE 2

// Constantes utilizadas en el cálculo de la frecuencia cadíaca
#define PERIODO_REFRACTARIO_INICIAL 300/INTERVALO_MUESTREO
#define UMBRAL 30

// Constantes utilizadas en el filtro pasa bajos
#define FC 20
#define DELTA_T 0.004
#define PI 3.141596


/*==================[internal data definition]===============================*/

// Variables para el manejo de la señal de ECG
uint16_t valor_analogico_leido = 0;
uint16_t salida_filtrada_previa = 0;
uint16_t salida_filtrada = 0;

// Variables para el cálculo de la frecuencia cardíaca
float frecuencia_cardiaca = 0;
uint16_t periodo_refractario = PERIODO_REFRACTARIO_INICIAL;
uint16_t contador_periodo = 0;
uint16_t mod_dif = 0;

// Estructuras para el conversor, puerto serie y timer
analog_input_config config_analog = {
								CANAL_ANALOGICO,
								MODO_ANALOGICO,
								&Leer_dato
								};

serial_config config_serie = {
						 //PUERTO_PC,
						 PUERTO_BLUETOOTH,
						 VELOCIDAD_TRANSFERENCIA,
						 NULL,
					   };

timer_config timer_analog = {
							TIMER_A,
							INTERVALO_MUESTREO,
							&AnalogStartConvertion
						  };




/*==================[external data definition]===============================*/



/*==================[external functions definition]==========================*/

// Filtro pasa bajos
uint16_t Filtrar(uint16_t entrada)
{
	float RC = 1/(2*PI*FC);
	float alfa = DELTA_T/( RC + DELTA_T );
	salida_filtrada_previa = salida_filtrada;
	salida_filtrada = salida_filtrada_previa + alfa*( entrada-salida_filtrada_previa);

}

// Cálculo de la frecuencia cardíaca
void Calcular_frecuencia()
{
	if (periodo_refractario == 0)
	{
	// Se calcula el módulo de la diferencia entre las salida actual y la anterior
	mod_dif = abs(salida_filtrada - salida_filtrada_previa);

	// Se compara si este valor es mayor a un umbral previamente establecido
		if(mod_dif > UMBRAL){

			frecuencia_cardiaca = 60000/(contador_periodo*INTERVALO_MUESTREO); // El 60000 es para obtener un valor en lat/min

			contador_periodo=0;

			periodo_refractario = PERIODO_REFRACTARIO_INICIAL;

			// Se cambia el color del indicador para mostrar la detección de onda R
			Color_Indicador(ONDA_R);

		}
	}
	else{
		periodo_refractario--;
	}
}

// Función que controla el color del indicador en la pantalla
void Color_Indicador(uint8_t estado){

	// El indicador espera el dato del color de la forma *LR#G#B#*
	UartSendString(config_serie.port,"*L");

	if (estado == ONDA_R){
		UartSendString(config_serie.port,"R0G255B0");	// Se pone el indicador en color verde
	}
	else if (estado == IDLE){
		UartSendString(config_serie.port,"R255G255B255"); // Se pone el indicador en color blanco
	}
	UartSendString(config_serie.port,"*");
}

// Función que envía los datos medidos y procesados por puerto serie hacia el módulo bluetooth
void Enviar_datos(){

	// Se envía el dato de la frecuencia cardíaca
	UartSendString(config_serie.port,"*f");
	UartSendString(config_serie.port,UartItoa(frecuencia_cardiaca,10));
	UartSendString(config_serie.port,"\n");
	UartSendString(config_serie.port,"*");

	// Se envía el dato de la señal filtrada. El valor -100 es solo un offset para la gráfica
	UartSendString(config_serie.port,"*G");
	UartSendString(config_serie.port,UartItoa(salida_filtrada-100,10));
	UartSendString(config_serie.port,",");

	// Se envía el dato de la señal sin filtrar. El valor -300 es solo un offset para la gráfica
	UartSendString(config_serie.port,UartItoa(valor_analogico_leido-300,10));
	UartSendString(config_serie.port,"*");
}

void Leer_dato(){

	AnalogInputRead(CANAL_ANALOGICO,&valor_analogico_leido);

	Filtrar(valor_analogico_leido);

	// Contador período se utiliza para el cálculo de la frecuancia cardíaca
	contador_periodo++;

	Calcular_frecuencia();

	// Se envían los datos medidos y procesados hacia el módulo bluetooth
	Enviar_datos();

	// Se comprueba si se esta por fuera del intervalo de detección de onda R y de ser así
	// se mantiene el indicador en color blanco. EL -30 es solo para dar un tiempo muerto
	// un poco mayor
	if (periodo_refractario<PERIODO_REFRACTARIO_INICIAL-30){

		Color_Indicador(IDLE);
	}

}



int main(void){

	SystemClockInit();

	LedsInit(); // Esto solamente para apagar los leds
	AnalogInputInit(&config_analog);
	UartInit(&config_serie);
	TimerInit(&timer_analog);
	TimerStart(TIMER_A);


    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

