/*! @mainpage Proyecto Final
 *
 * \section genDesc Descripción general
 *
 * El proyecto consiste en una aplicación que toma una señal de ECG utilizando una placa de biopotenciales, se procesa esta señal y se envía a través de un módulo bluetooth hacia un dispositivo móvil para ser visualizada en el mismo.
 *<br></br>
 *	En el  <a href="https://drive.google.com/file/d/1S5swo4NhLzwZKf8ulyL902Igf6VpxGFw/view?usp=sharing" target="_blank" >siguiente video</a> se puede visualizar el proyecto probado en el laboratorio de la Facultad.
 *	Y en <a href="https://drive.google.com/file/d/1-qlaU71Lj33cibJNR_aMvR65Sd40IZfB/view?usp=sharing"  target="_blank">este otro</a> se observa la captura de la aplicación.
 *
 *
 * \section hardConn Conexión de Hardware
 *
 *
 *
 * |   HC-05    	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	RX   	 	| 	RS232_TX	|
 * | 	TX  	 	| 	RS232_RX	|
 * | 	VCC 	 	| 	5V			|
 * | 	GND 	 	| 	GND			|
 *
 * |   Placa biopotenciales 	|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	VCC   	 	            | 	5V		    |
 * | 	GND  	 	            | 	GNDA     	|
 * | 	ECG 	 	            | 	CH1  		|
 *
 * <br></br>
 * @section changelog Registro de cambios
 *
 * |   Fecha    | Descripción                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/10/2021 | Creación del documento		                 |
 * | 15/11/2021 | Entrega del documento		           		     |
 *
 * @author Emiliano Riffel
 *
 */

#ifndef _Proyecto_Final_H
#define _Proyecto_Final_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _Proyecto_Final_H */

