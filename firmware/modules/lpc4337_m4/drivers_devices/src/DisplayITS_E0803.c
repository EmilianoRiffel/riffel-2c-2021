	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version Leando Medus
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 */

/*==================[inclusions]=============================================*/
#include "led.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
#define MAX_VALUE 999
#define MIN_VALUE 000
#define DIGITS 3
#define PIN_LINES 7
#define PORT_LINES 4
#define SELECT_LINES 3
/*==================[internal data declaration]==============================*/

uint16_t display_value = 0;
gpio_t port_lines [PORT_LINES] = {0};
gpio_t select_lines [SELECT_LINES] = {0};


/*==================[internal functions declaration]=========================*/

void BinaryToBcd (uint32_t data, uint8_t * bcd_number ) {
	int i ;
	uint32_t aux = data;

	for (i = DIGITS-1; i >= 0; i --)
	{
		bcd_number[i]= aux % 10;
		aux = aux / 10 ;
	}

}

void BCDTo7Seg(uint8_t *bcd) {

	int i, j;

	uint8_t mask = 0x001;

	for (i = 0; i < SELECT_LINES; i++)
	{

		for (j = 0; j < PORT_LINES; j++)
		{
			if (bcd[i] & mask)
			{
				GPIOOn(port_lines[j]);
			}
			else
			{
				GPIOOff(port_lines[j]);
			}

			mask = mask << 1;
		}

		mask = 0x001;

		GPIOOn(select_lines[i]);
		GPIOOff(select_lines[i]);

	}

}

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


/** \brief Function to turn on a specific led */


// gpio_t  pines [7] = {D1,D2,D3,D4,Sel_0,Sel_1,Sel_2}
/** \brief
 *   Función que inicializa el display en los pines dados.
 *   La función recibe un arreglo donde los 4 primeros valores
 *   son las líneas de datos y los últimos 3 son las líneas de selección.
 *	 Por ejemplo:
 *
 * 	    gpio_t  pins [7] = {D1,D2,D3,D4,Sel_0,Sel_1,Sel_2};
 *
 * 	    ITSE0803Init(pins);
 *
 * 	    */
bool ITSE0803Init(gpio_t * pins){

	int i;
	int j = 0;
	for(i = 0; i <  PIN_LINES ; i++ ){

		GPIOInit(pins[i],GPIO_OUTPUT);

		if (i < PORT_LINES) {
			port_lines [i] = pins[i];
		}
		else {
			select_lines [j] = pins[i];
			j++;
		}
	}

	return true;
}
/** \brief
 *  Función que coloca el valor pasado como parámetro en el display. El valor enviado
 *  debe estár entre 000 y 999.
 * */
bool ITSE0803DisplayValue(uint16_t valor){

	uint8_t bcd_number [DIGITS] = {0};

	// Se chequea que el valor este dentro de un determinado rango
	if(valor > MAX_VALUE)
		display_value = MAX_VALUE;
	else if (valor < MIN_VALUE)
		display_value = MIN_VALUE;
	else
		display_value = valor;

	// Se descompone el valor display_value y se lo almacena en bcd_number
	BinaryToBcd(display_value, bcd_number);

	// Se manda el número bcd al display
	BCDTo7Seg(bcd_number);

	return true;
}
/** \brief
 * Retorna el valor mostrado en el display.
 * */
uint16_t ITSE0803ReadValue(void){
	
	return display_value;
}
/** \brief
 * Función que libera los pines utilizados por el display.
 * */
bool ITSE0803Deinit(gpio_t * pins){

	GPIODeinit();
	return true;
}



/*==================[end of file]============================================*/
