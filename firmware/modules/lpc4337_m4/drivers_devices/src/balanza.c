/*==================[inclusions]=============================================*/
#include "analog_io.h"
#include "bool.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/


#define TENSION_REFERENCIA 5
#define MAXIMA_CAPACIDAD 150
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
uint16_t valor_analogico_leido = 0;
uint32_t peso_actual = 0;
analog_input_config config_analog;
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

 void Balanza_init(uint8_t canal_analogico,uint8_t modo_analogico){

	 config_analog.input = canal_analogico;
	 config_analog.mode = modo_analogico;

	AnalogInputInit(&config_analog);
 }

void Balanza_medir(){
	AnalogInputRead(config_analog.input,&valor_analogico_leido);

	// Se convierte el valor medido en un valor de peso. Se asume que el peso de la caja inicialmente es de 0 Kg
	peso_actual = (MAXIMA_CAPACIDAD/TENSION_REFERENCIA)*valor_analogico_leido;
}

uint32_t Balanza_obtener_peso(){
	return peso_actual;
}

/*==================[end of file]============================================*/
