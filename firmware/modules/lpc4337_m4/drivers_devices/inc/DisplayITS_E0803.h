/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef DISPLAYITS_08803_H
#define DisplayITS_E0803_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup DISPLAY Display ITS E0803
 ** @{ */

/** @brief This is a driver for the Display ITS E0803.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	ER			Emiliano Riffel
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210910 v0.1 initials initial version
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @fn bool ITSE0803Init(gpio_t * pins)
 * @brief Función que inicializa el display en los pines dados.
 * La función recibe un arreglo de la siguiente forma
 * 	gpio_t  pins [7] = {D1,D2,D3,D4,Sel_0,Sel_1,Sel_2}
 * @param [gpio_t pins&[]] La referencia al arreglo de pines.
 * @return TRUE si no se produjo error.
 */
bool ITSE0803Init(gpio_t * pins);

/** @fn bool ITSE0803DisplayValue(uint16_t valor)
 * @brief Coloca el valor pasado como parámetro en el display.
 * @param [uint16_t valor] El parámetro a mostrar.
 * @return TRUE si no se produjo error.
 */
bool ITSE0803DisplayValue(uint16_t valor);

/** @fn uint16_t ITSE0803ReadValue(void)
 * @brief Retorna el valor mostrado en el display.
 * @param[in] Sin parámetros.
 * @return uint16_t value
 */
uint16_t ITSE0803ReadValue(void);


/** @fn bool ITSE0803Deinit(gpio_t * pins)
 * @brief Releases the pins used by the display.
 * @param[in] Sin parámetros.
 * @return TRUE si no se produjo error.
 */
bool ITSE0803Deinit(gpio_t * pins);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef DisplayITS_E0803_H */

