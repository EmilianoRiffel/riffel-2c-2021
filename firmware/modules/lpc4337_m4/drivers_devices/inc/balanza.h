
#ifndef balanza_H
#define balanza_H

/** @brief This is a driver for the Display ITS E0803.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	ER			Emiliano Riffel
 */


/*==================[inclusions]=============================================*/
#include <stdint.h>


/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @fn void Balanza_init()
 * @brief Inicializa la balanza.
 */
void Balanza_init(uint8_t canal_analogico,uint8_t modo_analogico);

/** @fn void Balanza_medir()
 * @brief Realiza la medición del peso
 */
void Balanza_medir();

/** @fn uint32_t Balanza_obtener_peso();
 * @brief Retorna el peso medido por la balanza actualmente
 * @return uint16_t peso_medido
 */
uint32_t Balanza_obtener_peso();

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef balanza_H */

