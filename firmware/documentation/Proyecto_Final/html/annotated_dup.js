var annotated_dup =
[
    [ "analog_input_config", "structanalog__input__config.html", "structanalog__input__config" ],
    [ "creal32_T", "structcreal32___t.html", "structcreal32___t" ],
    [ "creal64_T", "structcreal64___t.html", "structcreal64___t" ],
    [ "creal_T", "structcreal___t.html", "structcreal___t" ],
    [ "digitalIO", "structdigital_i_o.html", "structdigital_i_o" ],
    [ "serial_config", "structserial__config.html", "structserial__config" ],
    [ "timer_config", "structtimer__config.html", "structtimer__config" ]
];