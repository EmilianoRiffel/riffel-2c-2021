/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*==================[macros and definitions]=================================*/



/*==================[internal functions declaration]=========================*/

typedef struct
	{
		uint8_t port; 	/*!< GPIO port number */
		uint8_t pin; 	/*!< GPIO pin number */
		uint8_t dir; 	/*!< GPIO direction ‘0’ IN; ‘1’ OUT */

	} gpioConf_t;


void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ) {
	int i ;
	uint32_t aux = data;

	for (i = digits-1; i >= 0; i --)
	{
		bcd_number[i]= aux % 10;
		aux = aux / 10 ;
	}

}


void BCDTo7Seg(uint8_t *bcd, gpioConf_t *vectBits, uint8_t vectBitsSize) {

	int i, j;

	uint8_t bcdSize = sizeof(bcd);
	uint8_t mask = 0x0001;

	for (i = 0; i < bcdSize; i++)  // Se recorre el vector bcd
	{
		printf(" TRABAJANDO sobre el dígito: %u \n\n", bcd[i]);

		for (j = 0; j < vectBitsSize; j++) // Se recorre el vector de struct
		{
			if (bcd[i] & mask << j)
			{
				printf("\t Se pone en 1 el pin %u del puerto %u \n",
						vectBits[j].pin, vectBits[j].port);
			}
			else
			{
				printf("\t Se pone en 0 el pin %u del puerto %u \n",
						vectBits[j].pin, vectBits[j].port);
			}
		}
		mask = 0x0001;
		printf("\n");
	}

}


int main(void)
 {

	gpioConf_t vectBits[4] = {
			{ 1, 4, 1 },     // PUERTO, PIN, DIR
			{ 1, 5, 1 },
			{ 1, 6, 1 },
			{ 2, 14, 1 }
	};

	uint8_t vectBitsSize = sizeof(vectBits) / sizeof(*vectBits);

	uint8_t bcd_number[4] = { 0 };
	uint32_t data = 1763;
	uint8_t digits = 4;

	BinaryToBcd(data, digits, bcd_number);

	printf("\n BCD: ");
	int i;
	for (i = 0; i < sizeof(bcd_number); i++) {
		printf("%u", bcd_number[i]);
	}
	printf("\n \n");

	BCDTo7Seg(bcd_number, vectBits, vectBitsSize);

	return 0;
}
/*==================[end of file]============================================*/

