/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
#define MASK_1 0x0000FF00;

struct _4bytes {
	uint8_t b1;
	uint8_t b2;
	uint8_t b3;
	uint8_t b4;
	};

union _4bytes_union{
		struct _4bytes every_byte;
		uint32_t  all_bytes;
	};


/*==================[internal functions declaration]=========================*/

int main(void)
{
	uint32_t variable = 0x01020304;
	uint8_t b1,b2,b3,b4;

	b1 = variable; 						 // primeros 8 bits
	b2 = (variable & 0x0000FF00 ) >> 8;  // segundos 8 bits
	b4 = variable >> 24; 				 // ultimos 8 bits
	b3 = (variable - b1 - b2 - 4) >>16;  // anteultimos 16 bits

	printf(" Utilizando máscaras \n \n");
	printf(" Primeros 8 bits: %u \n",b1);
	printf(" Segundos 8 bits: %u \n",b2);
	printf(" Anteultimos 8 bits: %u \n",b3);
	printf(" Ultimos 8 bits: %u \n",b4);

	printf("\n\n");



	union _4bytes_union bytes_un;

	bytes_un.all_bytes = variable;

	printf(" Utilizando union \n \n");
	printf(" Primeros 8 bits: %u \n",bytes_un.every_byte.b1);
	printf(" Segundos 8 bits: %u \n",bytes_un.every_byte.b2);
	printf(" Anteultimos 8 bits: %u \n",bytes_un.every_byte.b3);
	printf(" Ultimos 8 bits: %u \n",bytes_un.every_byte.b4);




	return 0;
}
/*==================[end of file]============================================*/

